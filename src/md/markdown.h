#ifndef QTMDEDIT_MARKDOWN_H
#define QTMDEDIT_MARKDOWN_H

#include <string>

namespace qtmedit::md {

std::string convert(const std::string& source);

} // namespace

#endif

