#include <catch2/catch.hpp>
#include "markdown.h"

TEST_CASE("Basic test", "[markdown]") {
    REQUIRE(qtmedit::md::convert("") == "<html><body></body></html>");
    REQUIRE(qtmedit::md::convert("anything") == "<html><body>anything</body></html>");
}

TEST_CASE("Bold", "[markdown]") {
    REQUIRE(qtmedit::md::convert("**bold**") == "<html><body><strong>bold</strong></body></html>");
}